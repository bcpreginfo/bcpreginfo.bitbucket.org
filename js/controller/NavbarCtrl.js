(function() {
    angular
        .module('App')
        .controller('NavbarCtrl', NavbarCtrl);

    NavbarCtrl.$inject = ['$scope', '$location', '$window'];

    function NavbarCtrl($scope, $location, $window) {
        $scope.getUrl = getUrl;
        function getUrl() {
            return $location.url().toString();
        }

        angular.element($window).bind("scroll", function() {
            var navbar = angular.element('.navbar.navbar-default');
            if (this.pageYOffset === 0) {
                navbar.removeClass('navbar-fixed-top');
                navbar.find('.navbar-right').css('margin-right','0');
                navbar.css('background', 'rgba(0, 0, 0, 0)');
            } else {
                if (!navbar.hasClass('navbar-fixed-top')){
                    navbar.addClass('navbar-fixed-top');
                    navbar.find('.navbar-right').css('margin-right','10px');
                    navbar.css('background', 'rgb(76,76,76)');
                }
            }
        });
    }
})();
