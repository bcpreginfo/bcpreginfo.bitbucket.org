(function() {
    angular
        .module('App')
        .controller('AboutUsCtrl', AboutUsCtrl);

    AboutUsCtrl.$inject = ['$scope'];

    function AboutUsCtrl($scope) {
        $scope.memberInView = memberInView;
  
        $scope.teamMembers = [
            {
                name: "Andy Chen",
                position: "Developer",
                email: "achen0115@gmail.com",
                github: "https://github.com/achen0115",
                linkedin: "https://www.linkedin.com/in/andy-chen",
                photo: "./imgs/team/andy-chen_cropped.jpg"
            },
            {
                name: "Jason Chien",
                position: "Developer",
                email: "chienchiawen@gmail.com",
                github: "https://github.com/cwc24",
                linkedin: "https://www.linkedin.com/in/jason-chien",
                photo: "./imgs/team/jason-chien_cropped.jpg"
            },
            {
                name: "Max Ou",
                position: "Developer",
                email: "maxo@linux.com",
                github: "",
                linkedin: "https://www.linkedin.com/in/max-ou",
                photo: "./imgs/team/max-ou_cropped.jpg"
            }
        ];

        function memberInView(index, isInview, info) {
            var item = info.element;
            if (isInview) {
                item.css('animation', 'bounce 1s');
            }
        }
    }
})();
