(function() {
    angular
        .module('App')
        .controller('TimelineDataCtrl', TimelineDataCtrl);

    TimelineDataCtrl.$inject = ['$scope'];

    function TimelineDataCtrl($scope) {
        $scope.isMSPTab = true;
        $scope.timelineItemInView = timelineItemInView;
        $scope.timelinePinInView = timelinePinInView;
        $scope.timelineItems = [
            {
                title: "Week 1-4",
                imageSrc: "./imgs/timeline/1.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "Pregnancy tests (can be purchased from shoppers and other retail store).", },
                    { text: "Purchase Materna - contains folic acid which can prevent baby becomes defected", },
                    { text: "Decide whether Midwife or OB for baby birth delivery.", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                ]
            },
            {
                title: "Week 5-8",
                imageSrc: "./imgs/timeline/2.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "Check for blood type", },
                    { text: "Check for AIDS is necessary and other test will be directed by your family physician if needed.", },
                    { text: "Physician will issue the reuistion form for the first ultrasound (Obstetrical B Scan).", },
                    { text: "First Ultrasound (Obstetrical B Scan) should be taken after 8 weeks to avoid Ectopic pregnancy.", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                    {
                        title: "Obstetrical B Scan (under 14 weeks gestation)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$80.18 or more*"
                        }
                    },
                    {
                        title: "Blood Firm Review",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$17.99 or more*"
                        }
                    }
                ]
            },
            {
                title: "Week 9-12",
                imageSrc: "./imgs/timeline/3.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "Physician will specifically give the date of the second ultrasound (Obstetrical B Scan).", },
                    { text: "Physician will advice you to take panorama test.", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                ]
            },
            {
                title: "Week 13-16",
                imageSrc: "./imgs/timeline/4.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "Optional - Panorama Test.", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                    {
                        title: "Panorama Prenatal test",
                        price: {
                            msp: "$550.0",
                            nomsp: "$550.0"
                        }
                    },
                    {
                        title: "Panorama Prenatal test additional - Deletion syndrome",
                        price: {
                            msp: "$195.0",
                            nomsp: "$195.0"
                        }
                    },
                    {
                        title: "Panorama Prenatal test additional - Microdeletion extended panel",
                        price: {
                            msp: "$245.0",
                            nomsp: "$245.0"
                        }
                    },
                ]
            },
            {
                title: "Week 17-20",
                imageSrc: "./imgs/timeline/5.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "Take second Ultrasound (Obstetrical B Scan) Test", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                    {
                        title: "Obstetrical B scan (14 weeks gestation or over)(for singles)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$106.86 or more*"
                        }
                    },
                    {
                        title: "Obstetrical B scan (14 weeks gestation or over) (for multiples – each additional fetus)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$79.52 or more*"
                        }
                    },
                    {
                        title: "Obstetrical B Scan less than 14 weeks with Nuchal Translucency measurement (for singles)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$123.26 or more*"
                        }
                    },
                    {
                        title: "Obstetrical B Scan less than 14 weeks with Nuchal Translucency measurement (for multiples – each additional fetus)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$92.44 or more*"
                        }
                    },
                ]
            },
            {
                title: "Week 21-24",
                imageSrc: "./imgs/timeline/6.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "Family physician will issue requistion form for Glucose Tolerance Test(GTT) test for checking diabetes.", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                ]
            },
            {
                title: "Week 25-28",
                imageSrc: "./imgs/timeline/7.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "Glucose Tolerance Test(GTT) test will be taken at this period.", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                    {
                        title: "Glucose Tolerance Test Gestational Protocol",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$15.84 or more*"
                        }
                    },
                    {
                        title: "Glucose Tolerance Test",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$12.94 or more*"
                        }
                    },
                    {
                        title: "Hermatology Profile",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$10.96 or more*"
                        }
                    }
                ]
            },
            {
                title: "Week 29-32",
                imageSrc: "./imgs/timeline/8.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "(First Pregnancy only) Your doctor will measure your bump to see how your baby's growing.", },
                    { text: "You'll also have a chance to chat through the test restuls from your 28-week appointment.", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                ]
            },
            {
                title: "Week 33-36",
                imageSrc: "./imgs/timeline/9.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "Your blood pressure and urine will be checked", },
                    { text: "Your bump will be measured to make sure your baby is growing well.", },
                    { text: "If your baby is breech the doctor may offer to make you an appointment to have your baby turned by hand.", },
                    { text: "Your care provider should talk to you about preparing for labor and birth.", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                ]
            },
            {
                title: "Week 37-40",
                imageSrc: "./imgs/timeline/10.jpg",
                content: [
                    { text: "Checkup with family physician.", },
                    { text: "Your blood pressure and urine will be checked.", },
                    { text: "The doctor should give you some information about your options if your pregnancy lasts longer than 41 weeks.", },
                ],
                fees: [
                    {
                        title: "Visit - in office (age 2-49)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$30.64 or more*"
                        }
                    },
                    {
                        title: "Visit - in office (age 50-59)",
                        price: {
                            msp: "Covered By MSP",
                            nomsp: "$33.70 or more*"
                        }
                    },
                ]
            },
        ];

        function timelineItemInView(index, isInview, info) {
            var item = info.element;
            if (isInview) {
                item.css('animation', 'pulse 1s');
            } else if (!isInview) {
                item.css('animation', '');
            }
        }

        function timelinePinInView(index, isInview, info) {
            var item = info.element;
            if (isInview) {
                item.css('animation', 'fadeIn 1s');
            } else if (!isInview) {
                item.css('animation', '');
            }
        }
    }
})();
