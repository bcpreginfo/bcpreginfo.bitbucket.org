(function() {
    angular
        .module('App')
        .controller('FoodsafetyCtrl', FoodsafetyCtrl);

    FoodsafetyCtrl.$inject = ['$scope'];
    
    function FoodsafetyCtrl($scope) {
        $scope.oneAtATime = true;

        $scope.groups = [
            {
                title: 'Materna',
                imageSrc: "./imgs/food-safety/materna_cropped.jpg",
                content: [
                    "Materna is a supplement contains folic acid and pre-natal multivitamins.",
                    "Folic acid helps prevent neural tube birth defects, which affect the brain and spinal cord.",
                    "It is recommended to start taking materna before 12 weeks of pregnancy for people who are planning to have baby.",
                    "Please aware of the folic acid has to be between 0.4 mg to 1.0 mg(ask your family physician before taking over or below the recommended amount).",            
                    "The multivitamins has the following effect:" +
                    "<ul><li>Help prevent iron deficiency.</li>"+
                    "<li>Help maintain maternal bone health.</li>"+
                    "<li>Help prevent vitamin D deficiency.</li></ul>"
                ],
                isSafe: true,
            },
            {
                title: 'Fish',
                imageSrc: "./imgs/food-safety/fish_cropped.jpg",
                content: [
                    "Fish is an excellent source of omega-3 fat, which is important for your baby's brain development.",
                    "Some fish is also high in mercury and high amounts of mercury can be harmful to a growing brain.",
                    "Choose fish that is low in mercury more often.",
                    "The following fish contains high mercury:" + 
                    "<ul><li>tuna</li>" +
                    "<li>shark</li>" + 
                    "<li>marlin</li>" +
                    "<li>swordfish</li>" + 
                    "<li>escolar</li>" +
                    "<li>orange roughy</ul></li>"
                ],
                isSafe: true,
            },
            {
                title: 'Vitamin A',
                imageSrc: "./imgs/food-safety/vitamin_a_cropped.jpg",
                content: [
                    "A balanced diet rich in dark green and orange vegetables and fruit provides enough vitamin A for the healthy development of your baby's skin, eyes and immune system.",
                    "Too much vitamin A may cause birth defects, especially during the first trimester.", 
                    "Do not take individual vitamin A or fish liver oil supplements during pregnancy.",
                    "Liver and liver products (e.g. liverwurst spread and liver sausages) are also high in vitamin A.", 
                    "The safest choice is to limit these foods during pregnancy. If you choose to eat liver products, had no more than 75g (2 1/2 ounces) per week.",
                    "Choose a prenatal supplement that has less than 10,000 IU (3000 mcg) of preformed vitamin A (often listed as acetate, succinate or palmitate)."
              ],
              isSafe: true,
            },
            {
                title: 'Caffeine',
                imageSrc: "./imgs/food-safety/caffeine_cropped.jpg",
                content: [
                    "Having too much caffeine during pregnancy may be harmful to your baby." + 
                    "Limit caffeine to 300 milligrams per day. 300 milligrams is about two cups (500 mL) of coffee or six cups of black tea. For more information visit <a class='food-content-hyperlink' href='http://www.hc-sc.gc.ca/fn-an/securit/addit/caf/food-caf-aliments-eng.php' target='_blank'>Health Canada</a>"
                ],
                isSafe: true,
            },
            {
                title: 'Alcohol',
                imageSrc: "./imgs/food-safety/alcohol_cropped.jpg",
                content: [
                    "No amount of alcohol has been shown to be safe during pregnancy.",
                    "The safest choice is to avoid alcohol during pregnancy.",
                    "'Fetal alcohol spectrum disorders' (FASD) is the term experts use to describe the range of problems related to alcohol exposure before birth.",
                    "The most severe result of alcohol use is fetal alcohol syndrome (FAS), a lifelong condition characterized by poor growth (in the womb, after birth, or both), <br/>abnormal facial features, heart defects, and damage to the central nervous system."
                ],
                isSafe: false,
            },
            {
                title: 'Herbal Products',
                imageSrc: "./imgs/food-safety/herbal_cropped.jpg",
                content: [
                    "Check with your health care provider if you are currently using herbal products in tablet, capsule or extract forms.",
                    "The following herbs are considered safe in the amounts commonly used in foods or as herbal tea (2-3 cups per day):" +
                    "<ul><li>ginger</li>" +
                    "<li>bitter orange/orange peel</li>" +
                    "<li>echinacea</li>" +
                    "<li>peppermint</li>" +
                    "<li>red raspberry leaf</li>" +
                    "<li>rose hip</li>" +
                    "<li>rosemary</li></ul>"
                ],
                isSafe: true,
            },
            {
                title: 'Sweeteners (Sugar Substitutes)',
                imageSrc: "./imgs/food-safety/sweetener_cropped.jpg",
                content: [
                    "Sugar substitutes are safe in moderation during pregnancy. However, it is important that foods and drinks made with these do not replace more nutritious options.",
                    "If you have concerns or questions about using sweeteners, talk to your health care provider or a dietitian."
                ],
                isSafe: true,
            },
            {
                title: 'Soy',
                imageSrc: "./imgs/food-safety/soy_cropped.jpg",
                content: [
                    "When part of a balanced diet soy foods and foods containing soy products are safe to eat during pregnancy.",
                    "Soy supplements are not recommended (for example, soy protein or isoflavone supplements)."
                ],
                isSafe: true,
            },
            {
                title: 'Flax',
                imageSrc: "./imgs/food-safety/flax_cropped.jpg",
                content: [
                    "Limit flaxseed and flaxseed oil to the amounts commonly found in foods.",
                    "It is not recommended to take flaxseed oil as a supplement."
                ],
                isSafe: false,
            }
        ];
    }
})();