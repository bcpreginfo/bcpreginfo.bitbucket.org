(function() {
    angular
        .module('App')
        .controller('ReferenceCtrl', ReferenceCtrl);

    ReferenceCtrl.$inject = ['$scope'];

    function ReferenceCtrl($scope) {
        $scope.references = [
            {
                title: "Healthlink BC",
                link: "https://www.healthlinkbc.ca/healthy-eating/pregnancy-food-safety"
            },
            {
                title: "Prenatal appointments scheduler - BabyCenter Canada",
                link: "http://www.babycenter.ca/t1013300/prenatal-appointments-scheduler"
            },
            {
                title: "Medical Services Commission Payment Schedule (December 1, 2016)",
                link: "http://www2.gov.bc.ca/assets/gov/health/practitioner-pro/medical-services-plan/msc-payment-schedule-december-2016.pdf"
            },
            {
                title: "Medical Services Commission Payment Schedule (October 1, 2015)",
                link: "http://www2.gov.bc.ca/assets/gov/health/practitioner-pro/laboratory-services/schedule_of_fees_-_laboratory_services_payment_schedule.pdf"
            },
        ];
    }
})();
