(function() {
    angular
        .module('App', ['angular-inview', 'ngAnimate', 'ngRoute', 'ui.bootstrap', 'ngSanitize']);
})();
