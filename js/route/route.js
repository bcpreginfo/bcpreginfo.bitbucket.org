(function() {
    angular
        .module('App')
        .config(route);

    function route($routeProvider, $locationProvider){
        $routeProvider
        .when("/", {
            templateUrl: "html/timeline.html",
            controller: "TimelineDataCtrl"
        })
        .when("/food_safety", {
            templateUrl: "html/food_safety.html",
            controller: "FoodsafetyCtrl"
        })
        .when("/about_us", {
            templateUrl: "html/about_us.html",
            controller: "AboutUsCtrl"
        });
    }
})();
